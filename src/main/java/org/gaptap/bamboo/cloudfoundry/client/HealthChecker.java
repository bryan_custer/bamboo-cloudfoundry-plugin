package org.gaptap.bamboo.cloudfoundry.client;

import reactor.core.publisher.Mono;

public interface HealthChecker {

    Mono<Void> assertHealthy(ApplicationConfiguration applicationConfiguration, BlueGreenConfiguration blueGreenConfiguration, Logger logger);
}
