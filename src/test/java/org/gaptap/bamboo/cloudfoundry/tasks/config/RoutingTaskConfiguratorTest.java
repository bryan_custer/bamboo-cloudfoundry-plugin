/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.collections.SimpleActionParametersMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import com.atlassian.struts.TextProvider;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.DELETE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.DOMAIN_BIND_SERVICE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.DOMAIN_ROUTE_ADD;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.DOMAIN_ROUTE_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.HOST_ROUTE_ADD;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.HOST_ROUTE_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.OPTION_ADD_ROUTE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.OPTION_BIND_ROUTE_SERVICE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.OPTION_DELETE_ROUTE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.PATH_ROUTE_ADD;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.PATH_ROUTE_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.SERVICE_NAME_BIND_SERVICE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RoutingTaskConfiguratorTest extends BaseCloudFoundryTaskConfiguratorTest {

    private RoutingTaskConfigurator configurator;

    private Map<String, Object> params;
    private ActionParametersMap actionParametersMap;

    @Before
    public void setup(){
        configurator = new RoutingTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        params = getBaseRequiredParameters();
        actionParametersMap = new SimpleActionParametersMap(params);
    }

    @Test
    public void aValidSelectedOptionMustBeProvided(){
        configurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(ApplicationTaskConfigurator.SELECTED_OPTION));
    }

    @Test
    public void whenCreatingARouteDomainAndHostAreRequired(){
        params.put(SELECTED_OPTION, OPTION_ADD_ROUTE);

        configurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(DOMAIN_ROUTE_ADD));
        assertNotNull(errorCollection.getErrors().get(HOST_ROUTE_ADD));
    }

    @Test
    public void whenCreatingARoutePathIsOptional(){
        params.put(SELECTED_OPTION, OPTION_ADD_ROUTE);
        params.put(DOMAIN_ROUTE_ADD, "cloudfoundry.org");
        params.put(HOST_ROUTE_ADD, "bamboo");

        assertTaskValidationHasNoErrors(configurator, actionParametersMap, errorCollection);
    }

    @Test
    public void whenCreatingARouteAndAPathIsProvidedItMustStartWithASlash(){
        params.put(SELECTED_OPTION, OPTION_ADD_ROUTE);
        params.put(DOMAIN_ROUTE_ADD, "cloudfoundry.org");
        params.put(HOST_ROUTE_ADD, "bamboo");
        params.put(PATH_ROUTE_ADD, "invalid");

        configurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(PATH_ROUTE_ADD));
    }

    @Test
    public void whenCreatingARouteAValidPathIsAccepted(){
        params.put(SELECTED_OPTION, OPTION_ADD_ROUTE);
        params.put(DOMAIN_ROUTE_ADD, "cloudfoundry.org");
        params.put(HOST_ROUTE_ADD, "bamboo");
        params.put(PATH_ROUTE_ADD, "/valid");

        assertTaskValidationHasNoErrors(configurator, actionParametersMap, errorCollection);
    }

    @Test
    public void whenDeletingARouteDomainAndHostAreRequired(){
        params.put(SELECTED_OPTION, OPTION_DELETE_ROUTE);

        configurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(DOMAIN_ROUTE_DELETE));
        assertNotNull(errorCollection.getErrors().get(HOST_ROUTE_DELETE));
    }

    @Test
    public void whenDeletingARoutePathIsOptional(){
        params.put(SELECTED_OPTION, OPTION_DELETE_ROUTE);
        params.put(DOMAIN_ROUTE_DELETE, "cloudfoundry.org");
        params.put(HOST_ROUTE_DELETE, "bamboo");

        assertTaskValidationHasNoErrors(configurator, actionParametersMap, errorCollection);
    }

    @Test
    public void whenDeletingARouteAndAPathIsProvidedItMustStartWithASlash(){
        params.put(SELECTED_OPTION, OPTION_DELETE_ROUTE);
        params.put(DOMAIN_ROUTE_DELETE, "cloudfoundry.org");
        params.put(HOST_ROUTE_DELETE, "bamboo");
        params.put(PATH_ROUTE_DELETE, "invalid");

        configurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(PATH_ROUTE_DELETE));
    }

    @Test
    public void whenDeletingARouteAValidPathIsAccepted(){
        params.put(SELECTED_OPTION, OPTION_DELETE_ROUTE);
        params.put(DOMAIN_ROUTE_DELETE, "cloudfoundry.org");
        params.put(HOST_ROUTE_DELETE, "bamboo");
        params.put(PATH_ROUTE_DELETE, "/valid");

        assertTaskValidationHasNoErrors(configurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theAddRouteFieldsAcceptBambooVariables(){
        params.put(ApplicationTaskConfigurator.SELECTED_OPTION, OPTION_ADD_ROUTE);
        params.put(DOMAIN_ROUTE_ADD, "${bamboo.domain}");
        params.put(HOST_ROUTE_ADD, "${bamboo.host}");
        params.put(PATH_ROUTE_ADD, "${bamboo.path}");

        assertTaskValidationHasNoErrors(configurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theAddRouteFieldsAcceptEmbeddedBambooVariables(){
        params.put(ApplicationTaskConfigurator.SELECTED_OPTION, OPTION_ADD_ROUTE);
        params.put(DOMAIN_ROUTE_ADD, "${bamboo.domain}.com");
        params.put(HOST_ROUTE_ADD, "${bamboo.host}-env");
        params.put(PATH_ROUTE_ADD, "${bamboo.path}-${bamboo.env}");

        assertTaskValidationHasNoErrors(configurator, actionParametersMap, errorCollection);
    }

    @Test
    public void whenBindingToARouteServiceDomainAndTheServiceNameAreRequired(){
        params.put(SELECTED_OPTION, OPTION_BIND_ROUTE_SERVICE);

        configurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(DOMAIN_BIND_SERVICE));
        assertNotNull(errorCollection.getErrors().get(SERVICE_NAME_BIND_SERVICE));
    }
}
