package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.collections.SimpleActionParametersMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.APP_NAME_SEARCH_REGEX;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.APP_NAME_SEARCH_VARIABLE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.DELETE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.MAP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.MAP_URI;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_APP_NAME_SEARCH;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_LIST;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_MAP;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_RENAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_UNMAP;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RENAME_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RENAME_NEWNAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.UNMAP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.UNMAP_URI;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class ApplicationTaskConfiguratorTest extends BaseCloudFoundryTaskConfiguratorTest{

    private ApplicationTaskConfigurator taskConfigurator;

    private Map<String, Object> params;
    private ActionParametersMap actionParametersMap;

    @Before
    public void setup(){
        taskConfigurator = new ApplicationTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        params = getBaseRequiredParameters();
        actionParametersMap = new SimpleActionParametersMap(params);
    }

    @Test
    public void aValidSelectedOptionMustBeProvided(){
        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(SELECTED_OPTION));
    }

    @Test
    public void theMappingFieldsAcceptsBambooVariables(){
        params.put(SELECTED_OPTION, OPTION_MAP);
        params.put(MAP_NAME, "${bamboo.mapName}");
        params.put(MAP_URI, "${bamboo.mapUri}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theMappingFieldsAcceptsEmbeddedBambooVariables(){
        params.put(SELECTED_OPTION, OPTION_MAP);
        params.put(MAP_NAME, "app-${bamboo.mapName}-${bamboo.suffix}");
        params.put(MAP_URI, "${bamboo.host}.${bamboo.domain}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theUnmappingFieldsAcceptsBambooVariables(){
        params.put(SELECTED_OPTION, OPTION_UNMAP);
        params.put(UNMAP_NAME, "${bamboo.mapName}");
        params.put(UNMAP_URI, "${bamboo.mapUri}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theUnmappingFieldsAcceptsEmbeddedBambooVariables(){
        params.put(SELECTED_OPTION, OPTION_UNMAP);
        params.put(UNMAP_NAME, "app-${bamboo.mapName}-${bamboo.suffix}");
        params.put(UNMAP_URI, "${bamboo.host}.${bamboo.domain}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theRenameFieldsAcceptsBambooVariables(){
        params.put(SELECTED_OPTION, OPTION_RENAME);
        params.put(RENAME_NAME, "${bamboo.name}");
        params.put(RENAME_NEWNAME, "${bamboo.newName}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theRenameFieldsAcceptsEmbeddedBambooVariables(){
        params.put(SELECTED_OPTION, OPTION_RENAME);
        params.put(RENAME_NAME, "app-${bamboo.name}-${bamboo.suffix}");
        params.put(RENAME_NEWNAME, "${bamboo.newNamePrevix}.${bamboo.domain}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theDeleteFieldsAcceptsBambooVariables(){
        params.put(SELECTED_OPTION, OPTION_DELETE);
        params.put(DELETE_NAME, "${bamboo.name}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theDeleteFieldsAcceptsEmbeddedBambooVariables(){
        params.put(SELECTED_OPTION, OPTION_DELETE);
        params.put(DELETE_NAME, "app-${bamboo.name}-${bamboo.suffix}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theAppNameSearchFieldsAcceptsBambooVariables(){
        params.put(SELECTED_OPTION, OPTION_APP_NAME_SEARCH);
        params.put(APP_NAME_SEARCH_REGEX, "${bamboo.regex}");
        params.put(APP_NAME_SEARCH_VARIABLE, "${bamboo.var}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void listApplicationsIsAValidOption(){
        params.put(SELECTED_OPTION, OPTION_LIST);

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }
}
